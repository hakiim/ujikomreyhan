<?php  
include 'koneksi.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>INVENTARIS!</title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery.min.js"></script>

<!-- favicon -->
<link rel="shortcut icon" href="images/inventaris.png" type="image/x-icon">
<link rel="icon" href="images/inventaris.png" type="image/x-icon">

</head>


<body class="nav-md">
<?php define('nav',TRUE); include 'tools/nav.php'; 
if (!isset($_SESSION['username'])) {
    header('location: login.php');
}
include 'koneksi.php';
?>


            <!-- page content -->
            <div class="right_col" role="main">
            	<div class="col-md-1"></div>
            	<div class="col-md-10">
            		<form action="proses_petugas.php" method="POST" enctype="multipart/form-data">
            			<div class="form-group">
            				<label>Username</label>
            				<input type="text" pattern="[A-za-z]{0,}" name="username" autocomplete="off" class="form-control" required/>
            			</div>
            			<div class="form-group">
            				<label>Password</label>
            				<input type="password" pattern="[A-za-z]{0,}" name="password" class="form-control" maxlength="25" md5 placeholder="" required/>
                            <input type="checkbox" id="showPass"> Show password
            			</div>
            			<div class="form-group">
            				<label>Nama Lengkap</label>
            				<input type="text" name="nama_petugas" pattern="[a-zA-Z\s]+" maxlength="35" autocomplete="off" class="form-control" required/>
            			</div>
            			<div class="form-group">
            				<label >Level</label>
                            <select class="form-control" tabindex="-1" name="id_level" >
                    <?php
                    $konek = mysqli_connect("localhost","root","","ujikom");
                    $level = mysqli_query ($konek,"SELECT * FROM level");
                    ?>
                    <?php foreach ($level as $a): ?>
                    <option value="<?php echo $a['id_level'] ?>"><?php echo $a['nama_level']?> </option>
                    <?php endforeach; ?>
                    </select>
            			</div>
                        <div class="form-group">
                            <label >Pegawai</label>
                            <select class="form-control" tabindex="-1" name="id_pegawai" >
                    <?php
                    $konek = mysqli_connect("localhost","root","","ujikom");
                    $pegawai = mysqli_query ($konek,"SELECT * FROM pegawai");
                    ?>
                    <?php foreach ($pegawai as $a): ?>
                    <option value="<?php echo $a['id_pegawai'] ?>"><?php echo $a['nama_pegawai']?> </option>
                    <?php endforeach; ?>
                    </select>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <input type="number" name="status" pattern="{0,}" autocomplete="off" class="form-control" required/>
                        </div>
            			<br>
            			<button class="btn btn-primary" type="submit" name="simpan">Simpan Perubahan</button>
                        <a class="btn btn-success" href="tampildata_petugas.php">Tampil Data Petugas</a>
            		</form>
            		<br/>

            	</div>
   			</div>
   

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

    <script src="js/custom.js"></script>

    <!-- flot js -->
    <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
    <script type="text/javascript" src="js/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.time.min.js"></script>
    <script type="text/javascript" src="js/flot/date.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.spline.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.stack.js"></script>
    <script type="text/javascript" src="js/flot/curvedLines.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.resize.js"></script>


    <!-- worldmap -->
    <script type="text/javascript" src="js/maps/jquery-jvectormap-2.0.1.min.js"></script>
    <script type="text/javascript" src="js/maps/gdp-data.js"></script>
    <script type="text/javascript" src="js/maps/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="js/maps/jquery-jvectormap-us-aea-en.js"></script>
   
   <script>
$(function(){
$("#showPass").click(function(){ // #showPass -> id Checkbox
if($("[name=password]").attr('type')=='password'){
$("[name=password]").attr('type','text');
}else{
$("[name=password]").attr('type','password');
}
});
});
</script> 
   
</body>

</html>
