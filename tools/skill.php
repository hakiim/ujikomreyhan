
<!-- skills -->
	<div class="skills">
		<div class="container">
			<div class="w3layouts_header w3_agile_head">
				<p><span><i class="fa fa-bullseye" aria-hidden="true"></i></span></p>
				<h5>Our <span>Skills</span></h5>
			</div>
			<div class="w3layouts_skills_grids">
				<div class="col-md-3 w3ls_about_guage">
					<h4>Make Money</h4>
					<canvas id="gauge1" width="200" height="100"></canvas>
				</div>
				<div class="col-md-3 w3ls_about_guage">
					<h4>Matching Buyer</h4>
					<canvas id="gauge2" width="200" height="100"></canvas>
				</div>
				<div class="col-md-3 w3ls_about_guage">
					<h4>Market Appraisals</h4>
					<canvas id="gauge3" width="200" height="100"></canvas>
				</div>
				<div class="col-md-3 w3ls_about_guage">
					<h4>Support</h4>
					<canvas id="gauge4" width="200" height="100"></canvas>
				</div>
				<div class="clearfix"> </div>
			</div>
		</div>
	</div>
<!-- //skills -->