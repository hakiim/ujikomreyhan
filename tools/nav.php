<?php
if(!defined('nav')){
echo('<script lenguage="javascript">
    window.alert("Halaman Ini Tidak Bisa Diakses");
    history.back();
    </script>');
    exit();
}
?>
<?php 
  session_start();
  include 'koneksi.php';
?>
<title> INVENTARIS! | </title>
    <div class="container body">


        <div class="main_container">

            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">

                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index.php" class="site_title"><i class="fa fa-desktop"></i> <span>INVENTARIS!</span></a>
                    </div>
                    <div class="clearfix"></div>

                    <!-- menu prile quick info -->
                    <div class="profile">
                        <div class="profile_pic">
                            <img src="images/admin.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome,</span>
                            <h2><?php 


                                     echo $_SESSION['nama_petugas'] ?></h2>
                        </div>
                    </div>
                    <!-- /menu prile quick info -->

                    <br />

                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                            <?php if ($_SESSION['id_level'] == 3) { ?>
                                <li><a href="peminjaman.php"><i class="fa fa-puzzle-piece"></i> Peminjaman </a></li>
                            <?php } elseif ($_SESSION['id_level'] == 2) { ?>
                                <li><a href="peminjaman.php"><i class="fa fa-puzzle-piece"></i> Peminjaman </a></li>
                                <li><a href="pengembalian.php"><i class="fa fa-home"></i> Pengembalian </a></li>
                            <?php }elseif ($_SESSION['id_level'] == 1) { ?>
                                <li><a href="inventaris.php"><i class="fa fa-cubes"></i> Inventaris </a></li>    
                                <li><a href="peminjaman.php"><i class="fa fa-puzzle-piece"></i> Peminjaman </a></li>
                                <li><a href="pengembalian.php"><i class="fa fa-home"></i> Pengembalian </a></li>
                                <li><a><i class="fa fa-database"></i> Master Data <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="input_petugas.php">Input Petugas</a>
                                        </li>
                                        <li><a href="input_pegawai.php">Input Pegawai</a>
                                        </li>
                                        <li><a href="input_jenis.php">Input Jenis</a></li>
                                        <li><a href="input_level.php">Input Level</a></li>
                                        <li><a href="input_ruang.php">Input Ruang</a></li>
                                    </ul>
                                <li><a><i class="fa fa-database"></i> Laporan <span class="fa fa-chevron-down"></span></a>
                                    <ul class="nav child_menu" style="display: none">
                                        <li><a href="laporan_inventaris.php">Laporan Inventaris</a>
                                        </li>
                                        <li><a href="input_pegawai.php">Input Pegawai</a>
                                        </li>
                                        <li><a href="input_jenis.php">Input Jenis</a></li>
                                        <li><a href="input_level.php">Input Level</a></li>
                                        <li><a href="input_ruang.php">Input Ruang</a></li>
                            <?php } else {
                                header('location: login.php');
                            }
                             ?>

                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->

                    <!-- /menu footer buttons -->
                    <div class="sidebar-footer hidden-small">
                        <a data-toggle="tooltip" data-placement="top" title="Settings">
                            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Lock">
                            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                        </a>
                        <a data-toggle="tooltip" data-placement="top" title="Logout">
                            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
                        </a>
                    </div>
                    <!-- /menu footer buttons -->
                </div>
            </div>

            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav class="" role="navigation">
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/admin.png" alt=""><?php echo $_SESSION['nama_pegawai'] ?>
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
                                    
                                    <li><a href="logout.php"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>

            </div>
            <!-- /top navigation -->