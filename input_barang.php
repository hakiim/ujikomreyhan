<?php  
include 'koneksi.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>INVENTARIS!</title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery.min.js"></script>

<!-- favicon -->
<link rel="shortcut icon" href="images/inventaris.png" type="image/x-icon">
<link rel="icon" href="images/inventaris.png" type="image/x-icon">

</head>


<body class="nav-md">
<?php define('nav',TRUE); include 'tools/nav.php'; 
if (!isset($_SESSION['username'])) {
    header('location: login.php');
}
include 'koneksi.php';
?>


            <!-- page content -->
            <div class="right_col" role="main">
            	<div class="col-md-1"></div>
            	<div class="col-md-10">
                        <?php
     
                include "koneksi.php";
                $query ="SELECT max(kode_inventaris) as maxKode FROM inventaris";
                $hasil = mysqli_query($koneksi,$query);
                $data =  mysqli_fetch_array($hasil);
                $kode_inventaris = $data['maxKode'];
                $noUrut = (int) substr($kode_inventaris, 3, 3);
                $noUrut++;
                $char = "K";
                $kode_inventaris = $char . sprintf("%03s", $noUrut);
                
                ?>
            		<form action="proses_barang.php" method="POST" enctype="multipart/form-data">
            			<div class="form-group">
            				<label>Nama</label>
            				<input type="text" pattern="[A-za-z]{0,}" name="nama" autocomplete="off" class="form-control" required/>
            			</div>
                        <div class="form-group">
                            <label>Kondisi</label>
                            <input type="text" pattern="[A-za-z]{0,}" name="kondisi" autocomplete="off" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label>Keterangan</label>
                            <input type="text" name="keterangan" autocomplete="off" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label>Jumlah</label>
                            <input type="number" name="jumlah" autocomplete="off" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label >Jenis</label>
                            <select class="form-control" tabindex="-1" name="id_jenis" >
                    <?php
                    $konek = mysqli_connect("localhost","root","","ujikom");
                    $jenis = mysqli_query ($konek,"SELECT * FROM jenis");
                    ?>
                    <?php foreach ($jenis as $a): ?>
                    <option value="<?php echo $a['id_jenis'] ?>"><?php echo $a['nama_jenis']?> </option>
                    <?php endforeach; ?>
                    </select>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Register</label>
                            <input type="date" name="tanggal_register" class="form-control" required/>
                        </div>
                          <div class="form-group">
                            <label>Ruang</label>
                            <select class="form-control" tabindex="-1" name="id_ruang" >
                    <?php
                    $konek = mysqli_connect("localhost","root","","ujikom");
                    $ruang = mysqli_query ($konek,"SELECT * FROM ruang");
                    ?>
                    <?php foreach ($ruang as $a): ?>
                    <option value="<?php echo $a['id_ruang'] ?>"><?php echo $a['nama_ruang']?> </option>
                    <?php endforeach; ?>
                    </select>
                        </div>
                        <div class="form-group">
                            <label>Kode Inventaris</label>
                            <input type="text" name="kode_inventaris" 
                            value="<?php echo $kode_inventaris;?>" autocomplete="off" readonly class="form-control" required/>
                        </div>
            			<div class="form-group">
                            <label>Petugas</label>
                            <select class="form-control" tabindex="-1" name="id_petugas" >
                    <?php
                    $konek = mysqli_connect("localhost","root","","ujikom");
                    $petugas = mysqli_query ($konek,"SELECT * FROM petugas");
                    ?>
                    <?php foreach ($petugas as $b): ?>
                    <option value="<?php echo $b['id_petugas'] ?>"><?php echo $b['nama_petugas']?> </option>
                    <?php endforeach; ?>
                    </select>
                        </div>
            
            			<br>
            			<button class="btn btn-primary" type="submit" name="simpan">Simpan Perubahan</button>
                        <a class="btn btn-success" href="inventaris.php">Tampil Data Barang</a>
            		</form>
            		<br/>

            	</div>
   			</div>
   

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

    <script src="js/custom.js"></script>

    <!-- flot js -->
    <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
    <script type="text/javascript" src="js/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.time.min.js"></script>
    <script type="text/javascript" src="js/flot/date.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.spline.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.stack.js"></script>
    <script type="text/javascript" src="js/flot/curvedLines.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.resize.js"></script>


    <!-- worldmap -->
    <script type="text/javascript" src="js/maps/jquery-jvectormap-2.0.1.min.js"></script>
    <script type="text/javascript" src="js/maps/gdp-data.js"></script>
    <script type="text/javascript" src="js/maps/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="js/maps/jquery-jvectormap-us-aea-en.js"></script>
    
   
</body>

</html>
