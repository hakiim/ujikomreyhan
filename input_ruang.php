<?php  
include 'koneksi.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>INVENTARIS!</title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery.min.js"></script>

<!-- favicon -->
<link rel="shortcut icon" href="images/inventaris.png" type="image/x-icon">
<link rel="icon" href="images/inventaris.png" type="image/x-icon">

</head>


<body class="nav-md">
<?php define('nav',TRUE); include 'tools/nav.php'; 
if (!isset($_SESSION['username'])) {
    header('location: login.php');
}
include 'koneksi.php';
?>


            <!-- page content -->
            <div class="right_col" role="main">
            	<div class="col-md-1"></div>
            	<div class="col-md-10">
            		<form action="proses_ruang.php" method="POST" enctype="multipart/form-data">
            			<div class="form-group">
            				<label>Nama Ruang</label>
            				<input type="text" pattern="[A-za-z]{0,}" name="nama_ruang" autocomplete="off" class="form-control" required/>
            			</div>
            			<div class="form-group">
            				<label>Kode Ruang</label>
            				<input type="text" name="kode_ruang" autocomplete="off" class="form-control" required/>
            			</div>
            			<div class="form-group">
            				<label>Keterangan</label>
            				<input type="text" pattern="[A-za-z]{0,}" name="keterangan" autocomplete="off" class="form-control" required/>
            			</div>
            			<br>
            			<button class="btn btn-primary" type="submit" name="simpan">Simpan Perubahan</button>
                        <a class="btn btn-success" href="tampildata_ruang.php">Tampil Data Ruang</a>
            		</form>
            		<br/>

            	</div>
   			</div>
   

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

    <script src="js/custom.js"></script>

    <!-- flot js -->
    <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
    <script type="text/javascript" src="js/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.time.min.js"></script>
    <script type="text/javascript" src="js/flot/date.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.spline.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.stack.js"></script>
    <script type="text/javascript" src="js/flot/curvedLines.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.resize.js"></script>


    <!-- worldmap -->
    <script type="text/javascript" src="js/maps/jquery-jvectormap-2.0.1.min.js"></script>
    <script type="text/javascript" src="js/maps/gdp-data.js"></script>
    <script type="text/javascript" src="js/maps/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="js/maps/jquery-jvectormap-us-aea-en.js"></script>
   
   <script>
$(function(){
$("#showPass").click(function(){ // #showPass -> id Checkbox
if($("[name=password]").attr('type')=='password'){
$("[name=password]").attr('type','text');
}else{
$("[name=password]").attr('type','password');
}
});
});
</script> 
   
</body>

</html>
