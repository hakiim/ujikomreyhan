<?php  
include 'koneksi.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>INVENTARIS!  </title>

<!-- DataTables CSS -->
    <link href="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="assets/css/jquery.dataTables.css">

    <!-- favicon -->
<link rel="shortcut icon" href="images/inventaris.png" type="image/x-icon">
<link rel="icon" href="images/inventaris.png" type="image/x-icon">


</head>
    
    <?php define('nav',TRUE); include 'tools/nav.php'; ?>

<body class="nav-md">
  <div class="right_col" role="main">
        <div class="col-md-12"></div>
        <div class="col-md-10">
            <form class="form-inline" action="" method="GET" enctype="multipart/form-data">
                <div class="form-group">
                    <label>Nama Pegawai</label>
                    <select class="form-control" tabindex="-1" name="id_pegawai">
                        <?php 
                        $nama_pegawai=$_SESSION['nama_pegawai'];
                        $query=mysqli_query($koneksi,"select id_pegawai from pegawai where nama_pegawai='$nama_pegawai'");
                        $data=mysqli_fetch_array($query);
                        ?>
                       <option value="<?php echo $data['id_pegawai'];?>"><?php echo $nama_pegawai?></option>
                        </select>
                </div>
                <center><div class="form-group">
                    <label>Nama Barang</label>
                    <select class="form-control" tabindex="-1" name="id_inventaris" >
                        <?php
                        $inventaris = mysqli_query($koneksi,"SELECT * FROM inventaris");
                        ?>
                        <?php foreach ($inventaris as $a): ?>
                            <option value="<?php echo $a['id_inventaris'] ?>"><?php echo $a['nama']?> </option>
                        <?php endforeach; ?>
                    </select>
                </div></center><br>
                <center>
                    <button class="btn btn-primary" type="submit">Cek Barang</button>
                </center>
            </form>
            <?php
            if(isset($_GET['id_pegawai']) && isset($_GET['id_inventaris'])){?>
            <form action="proses_peminjaman.php" method="post" enctype="multipart/form-data">
               <?php
               include "koneksi.php";
               $id_inventaris=$_GET['id_inventaris'];
               $select=mysqli_query($koneksi,"select * from inventaris where id_inventaris='$id_inventaris'");
               $data=mysqli_fetch_array($select);
                ?>
                <br>
                <br>
                <div class="row">
                  <input name="id_pegawai" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $_GET['id_pegawai'];?>" autocomplete="off" maxlength="11" required="">
                  <input name="id_inventaris" type="hidden" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['id_inventaris'];?>" autocomplete="off" maxlength="11" required="">
                  <div class="col-md-3">Nama<input name="nama" type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $data['nama'];?>" autocomplete="off" maxlength="11" readonly></div>
                  <div class="col-md-3">Jumlah Tersedia<input name="jumlah" type="text" class="form-control" value="<?php echo $data['jumlah'];?>" autocomplete="off" maxlength="11" required="" readonly></div>
                  <div class="col-md-3">Kode Barang<input name="kode_inventaris" type="text" class="form-control" value="<?php echo $data['kode_inventaris'];?>" autocomplete="off" maxlength="11" required="" readonly></div>
                  <div class="col-md-3">Jumlah Pinjam<input name="jumlah_pinjam" type="number" class="form-control" placeholder="Jumlah" autocomplete="off" min="1" max="<?php echo $data['jumlah'];?>"></div>
                  <br><br><br>
                  <br><div style="margin-left: 10px"><button type="submit" class="btn">Pinjam</button></div>
                  <br>
                  <br>

              </form>

            <?php 
            $status_peminjaman="Pinjam";
            ?>
            <br><br>
            <div class="row">
              <div class="panel-body">
                  <div class="table-responsive">

                    <div class="col-lg-12">
                      <?php 
                      $tanggal_pinjam=date("Y-m-d G:i:s", time()+60*60*6);
                      ?>
                      <div class="row" style="margin-left: 550px">
                        <div class="col-md-7">Tanggal Pinjam<input name="tanggal_pinjam" readonly type="text" class="form-control" placeholder="Masukan ID Inventaris" value="<?php echo $tanggal_pinjam;?>" autocomplete="off" maxlength="11"></div>
                    </div><br>
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Jumlah Pinjam</th>
                                <th>Nama Pegawai</th>
                                <th>Option</th>

                            </tr> 
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            $select=mysqli_query($koneksi,"select t.*,p.*,i.*,t.jumlah from temp_peminjaman t JOIN inventaris i ON t.id_inventaris=i.id_inventaris JOIN pegawai p ON t.id_pegawai=p.id_pegawai where t.id_pegawai='$_GET[id_pegawai]'");
                            while($data=mysqli_fetch_array($select))
                            {
                                ?>
                                <tr>
                                    <td><?=$no++?></td>
                                    <td><?=$data['nama']?></td>
                                    <td><?=$data['jumlah']?></td>
                                    <td><?=$data['nama_pegawai']?></td>
                                    <td>
                                    <a class="btn btn outline btn-info  glyphicon glyphicon-trash" href="hapus_temp_peminjaman.php?id=<?php echo $data['id'] ?>&&id_inventaris=<?php echo $data['id_inventaris']?>"></a></td>

                               </tr>

                                       <?php
                                   }
                                   ?>

                           </tbody>
                       </table>
                       <br>
                       <a href="checkout.php?id_pegawai=<?=$_GET['id_pegawai']?>" class="btn btn-warning">&nbsp;Pinjam</a>
                       <hr><br>
                       <br>

                <br>

            <br/>

        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
              <?php } ?>
            <!-- page content -->
            <div class="right_col">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                Data Semua Peminjam
                                </div>
                                <div class="table-responsive">
                                <div class="panel-body">
 <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal Pinjam</th>
                            <th>Status Peminjaman</th>
                            <th>Pegawai</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
        include 'koneksi.php';
        $no = 1;
        $data = mysqli_query($koneksi,"SELECT * from peminjaman JOIN pegawai ON peminjaman.id_pegawai = pegawai.id_pegawai where status_peminjaman='Dipinjam'");
        while($d = mysqli_fetch_array($data)){
            ?>
                    
                        <tr class="success">
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $d['tanggal_pinjam'] ?></td>
                            <td><?php echo $d['status_peminjaman'] ?></td>
                            <td><?php echo $d['nama_pegawai'] ?></td>
                            <td><a class="btn btn-info " href="edit_pengembalian.php?id=<?php echo $d['id']; ?>&id_pegawai=<?php echo $d ['id_pegawai'];?>">Kembalikan</a>
                        </tr>
                        <?php 
        }
        ?>
                </table>
                <a href="laporan_pdfinventaris.php" class="btn btn-danger">Print To PDF</a>
            <a href="prosesexcel_inventaris.php" class="btn btn-success fa fa-sign-out">&nbsp;Eksport Data ke-Excel</a>
                <script type="text/javascript" src="assets/js/jquery.min.js"></script>
                                <script type="text/javascript" src="assets/js/jquery.dataTables.min.js"></script>
                                <script>
                                $(document).ready(function()
                                {
                                $('#example').DataTable();
                                });
                                </script>
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>


               
    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

    <script src="js/custom.js"></script>

    <!-- flot js -->
    <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
    <script type="text/javascript" src="js/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.time.min.js"></script>
    <script type="text/javascript" src="js/flot/date.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.spline.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.stack.js"></script>
    <script type="text/javascript" src="js/flot/curvedLines.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.resize.js"></script>

 <!-- DataTables JavaScript -->
    <script src="bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- worldmap -->
    <script type="text/javascript" src="js/maps/jquery-jvectormap-2.0.1.min.js"></script>
    <script type="text/javascript" src="js/maps/gdp-data.js"></script>
    <script type="text/javascript" src="js/maps/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="js/maps/jquery-jvectormap-us-aea-en.js"></script>

    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
    <script src="js/jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='bower_components/moment/min/moment.min.js'></script>
<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='js/jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="js/jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="js/jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="js/jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="js/jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="js/jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="js/jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="js/charisma.js"></script>
   
</body>

</html>