<?php  
include 'koneksi.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>INVENTARIS!  </title>

    <!-- Bootstrap core CSS -->

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="css/icheck/flat/green.css" rel="stylesheet" />
    <link href="css/floatexamples.css" rel="stylesheet" type="text/css" />

    <script src="js/jquery.min.js"></script>

    <!-- favicon -->
<link rel="shortcut icon" href="images/inventaris.png" type="image/x-icon">
<link rel="icon" href="images/inventaris.png" type="image/x-icon">


</head>
    
    <?php define('nav',TRUE); include 'tools/navindex.php'; ?>

<body class="nav-md">

<br>
<br>
<br>
            <!-- page content -->
            <div class="right_col" role="main">
            <h1 align="center" >Website Inventaris</h1><br>
            <h2>A.    Pengertian Inventarisasi Sarana dan Sarana Pendidikan 
Inventarisasi berasal dari kata “inventaris” (Latin = inventarium) yang berarti daftar barang-barang, bahan dan sebagainya. Inventarisasi sarana dan prasarana pendidikan adalah pencatatan atau pendaftaran barang-barang milik sekolah ke dalam suatu daftar inventaris barang secara tertib dan teratur menurut ketentuan dan tata cara yang berlaku. Barang inventaris sekolah adalah semua barang milik negara (yang dikuasai sekolah) baik yang diadakan/dibeli melalui dana dari pemerintah, DPP maupun diperoleh sebagai pertukaran, hadiah atau hibah serta hasil usaha pembuatan sendiri di sekolah guna menunjang kelancaran proses belajar mengajar.
Tiap sekolah wajib menyelenggarakan inventarisasi barang milik negara yang dikuasai/diurus oleh sekolah masing-masing secara teratur, tertib dan lengkap. Kepala sekolah melakukan dan bertanggung jawab atas terlaksananya inventarisasi fisik dan pengisian daftar inventaris barang milik negara yang ada di sekolahnya.<br><br>

B.    Tujuan Inventarisasi Sarana dan Sarana Pendidikan
Secara umum, inventarisasi dilakukan dalam rangka usaha penyempurnaan pengurusan dan pengawasan yang efektif terhadap sarana dan prasarana yang dimiliki oleh suatu sekolah. Secara khusus, inventarisasi dilakukan dengan tujuan-tujuan sebagai berikut:


Untuk menjaga dan menciptakan tertib administrasi sarana dan prasarana yang dimiliki oleh suatu sekolah.
Untuk menghemat keuangan sekolah baik dalam pengadaan maupun untuk pemeliharaan dan penghapusan sarana dan prasarana sekolah.
Sebagai bahan atau pedoman untuk menghitung kekayaan suatu sekolah dalam bentuk materil yang dapat dinilai dengan uang.
Untuk memudahkan pengawasan dan pengendalian sarana dan prasarana yang dimiliki oleh suatu sekolah.<br><br>


C.    Manfaat Inventarisasi Sarana dan Prasarana Pendidikan
Daftar inventarisasi barang yang disusun dalam suatu organisasi yang lengkap, teratur dan berkelanjutan dapat memberikan manfaat, yakni sebagai berikut:

Menyediakan data dan informasi dalam rangka menentukan kebutuhan dan menyusun rencana kebutuhan barang.
Memberikan data dan informasi untuk dijadikan bahan/pedoman dalam pengarahan pengadaan barang.
Memberikan data dan informasi untuk dijadikan bahan/pedoman dalam penyaluran barang.
Memberikan data dan informasi dalam menentukan keadaan barang (tua, rusak, lebih) sebagai dasar untuk menetapkan penghapusannya.
Memberikan data dan informasi dalam rangka memudahkan pengawasan dan pengendalian barang </h2>
            </div>
   

    <script src="js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="js/chartjs/chart.min.js"></script>
    <!-- bootstrap progress js -->
    <script src="js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="js/nicescroll/jquery.nicescroll.min.js"></script>
    <!-- icheck -->
    <script src="js/icheck/icheck.min.js"></script>
    <!-- daterangepicker -->
    <script type="text/javascript" src="js/moment.min.js"></script>
    <script type="text/javascript" src="js/datepicker/daterangepicker.js"></script>

    <script src="js/custom.js"></script>

    <!-- flot js -->
    <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
    <script type="text/javascript" src="js/flot/jquery.flot.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.pie.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.orderBars.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.time.min.js"></script>
    <script type="text/javascript" src="js/flot/date.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.spline.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.stack.js"></script>
    <script type="text/javascript" src="js/flot/curvedLines.js"></script>
    <script type="text/javascript" src="js/flot/jquery.flot.resize.js"></script>

 <!-- DataTables JavaScript -->
    <script src="bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <!-- worldmap -->
    <script type="text/javascript" src="js/maps/jquery-jvectormap-2.0.1.min.js"></script>
    <script type="text/javascript" src="js/maps/gdp-data.js"></script>
    <script type="text/javascript" src="js/maps/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="js/maps/jquery-jvectormap-us-aea-en.js"></script>

    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
   
</body>

</html>