Nama	: Reyhan Hakiim
Kelas	: XII RPL 2


WEB INVENTARIS SARANA DAN PRASARANA SMK dalam web ini terdapat 3 hak akses didalamnya :
1.Admin
Dalam web administrator terdapat 6 menu diantaranya :
1.1 Dashboard
1.2 Inventaris
    1.2.1 Data Barang 
	  didalam menu ini terdapat sekumpulan data barang mulai dari kondisi barang dsb.
    1.2.2 Data Jenis
	  didalam menu ini terdapat sekumpulan data jenis - jenis barang.
    1.2.3 Data Ruang
	  didalam menu ini terdapat sekumpulan data ruang,yang dimana barang tersebut disimpan.
1.3 Peminjaman
    didalam menu peminjaman ini terdapat data barang yang akan di pilih untuk dipinjam oleh pegawai,dan dapat meminjam barang lebih dari 1.
1.4 Pengembalian
    didalam menu pengembalian ini terdapat data barang yang telah dipinjam yang akan di kembalikan.
1.5 Laporan
    1.5.1 Laporan Barang
          didalam menu laporan ini terdapat data barang yang akan di cetak per tanggalnya.
    1.5.2 Laporan Peminjaman
          didalam menu laporan ini terdapat data peminjaman barang yang di cetak bisa melalui PDF & EXCEL.
1.6 Lainnya
    1.6.1 Petugas
          didalam menu ini terdapat data untuk masuk/login ke web ini seperti username dan password.
    1.6.2 Pegawai
          didalam menu ini terdapat data pegawai yang akan meminjam barang.

2.Operator
dalam web operator hanya terdapat 3 menu didalamnya :
2.1 Dashboard
2.2 Peminjaman
    didalam menu ini terdapat data barang yang akan dipinjam oleh pegawai, dan bisa dipinjam lebih dari 1 orang.
2.3 Pengembalian
    didalam menu ini terdapat data barang yang telah di pinjam yang akan dikembalikan.

3.Peminjam
dalam web peminjam hanya terdapat 1 menu saja :
3.1 Peminjaman
    didalam menu ini terdapat data barang yang akan dipinjam oleh si peminjam.
    



Penggunaan Pada Web INVENTARIS SARANA DAN PRASARANA :
1. Sebagai Admin
    1.1 Langkah Pertama, Admin dapat memasukkan Username dan Password
    1.2 Setelah Username dan Password berhasil masuk lalu akan diarahkan ke halaman Dashboard dan Admin dapat melihat menu-menu yang ada di web admin.
    1.3 Admin dapat CREATE, READ, UPDATE dan DELETE (CRUD).

2. Sebagai Operator
    2.1 Langkah Pertama, User dapat memasukkan Username dan Password
    2.2 setelah Username dan Password berhasil masuk lalu akan diarahkan ke halaman Dashboard dan User dapat melihat, meminjam dan bisa mengembalikan barang yang telah di pinjam.

3. Sebagai Peminjam
    3.1 Langkah Pertama, User dapat memasukkan Username dan Password
    3.2 setelah Username dan Password berhasil masuk lalu akan diarahkan ke halaman Peminjaman yang akan di pinjam oleh pegawai.
    	* Langkah pertama yaitu memilih Nama Barang yang akan dipinjam
   	* Ketika sudah muncul data yang akan dipinjam, langkah selanjutnya adalah mengisi jumlah barang yang akan dipinjam
    	* Jika ingin meminjam barang lebih dari satu, silahkan pilih lagi nama barang yang akan di pinjam.

# Hak Akses Administrator
  Username : admin
  Password : admin

# Hak Akses Operator
  Username : operator
  Password : operator1

# Hak Akses Peminjam
  Username : peminjam
  Password : peminjam


