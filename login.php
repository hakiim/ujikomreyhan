<?php
session_start();
include "koneksi.php";
    
    if (isset($_SESSION['username'])) {
        header('location:index.php');
    }
?>
<html lang="en">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>INVENTARIS</title>

    <!-- Bootstrap core CSS -->

    <link href="css3/bootstrap.min.css" rel="stylesheet">

    <link href="fonts/css3/font-awesome.min.css" rel="stylesheet">
    <link href="css3/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="css3/custom.css" rel="stylesheet">
    <link href="css3/icheck/flat/green.css" rel="stylesheet">

    <!-- favicon -->
    <link rel="shortcut icon" href="images/inventaris.png" type="image/x-icon">
    <link rel="icon" href="images/inventaris.png" type="image/x-icon">

    <script src="js/jquery.min.js"></script>
<?php define('header',TRUE);  include "tools/banner.php"; ?>
</head>

<body bgcolor="black">
		<a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>
		
        <div id="wrapper">
            <div id="login" class="animate form">
                <section class="login_content">
                    <form role="login_form" action="ceklogin.php" method="post">
                        <h1>Login </h1>
                        <div>
                            <input type="text" class="form-control" maxlength="10" name="username" placeholder="Username" autocomplete="off" required="" />
                        </div>
                        <div>
                            <input type="password" class="form-control" maxlength="10" name="password" placeholder="Password" required="" />
                        </div>
                        <div>
						
                            <input type="submit" name="login" value="Login" class="btn btn-default submit" >
				            
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">
                            </p>
                            <div class="clearfix"></div>
                            <div>
                                <p>Aplikasi Inventaris Sarana dan Prasarana di SMK</p>
                                <p>©2019 All Rights Reserved. </p>
                            </div>
                        </div>
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
            </div>
           
        </div>
    </div>

</body>

</html>